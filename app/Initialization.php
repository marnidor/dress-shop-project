<?php

namespace App;

class Initialization
{
    public static function start()
    {
        self::connectLibs();
        self::connectDB();
    }

    private static function connectLibs()
    {
        $config = require_once 'config/config.php';
        foreach ($config['libs'] as $lib) {
            require_once 'libs/' . $lib . '.php';
        }
    }

    private static function connectDB()
    {
        $config = require_once 'config/db.php';
        if ($config['enable']) {
            \R::setup('mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'] . '', $config['user'], $config['password']);
            if (!\R::testConnection()) {
                die('Ошибка подключения к базе данных');
            }
        }
    }
}
