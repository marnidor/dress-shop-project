<?php

use App\services\Router;

Router::openPage('/', 'home');
Router::openPage('/registration', 'registration');
Router::openPage('/eveningDressesCollection', 'eveningDressesCollection');
Router::openPage('/cocktailDressesCollection', 'coctailDressesCollection');
Router::openPage('/midiDressesCollection', 'midiDressesCollection');
Router::openPage('/login', 'login');
Router::openPage('/userProfile', 'userProfile');
Router::openPage('/aboutBrand', 'aboutBrand');

Router::post('/registration/upload', \App\registration\Registration::class, 'getDataFromRegistration', true, true);
Router::post('/login/upload', \App\registration\Registration::class, 'getDataFromLogin', true, false);

Router::compareUri();
